# -*- coding: utf-8 -*-
import base64
import os
import subprocess
from shutil import copyfile
import boto3
from PIL import Image
from botocore.client import Config
import json
import pusher
import uuid


FAST_INC = "fast_inc.png"
CAIRE = "caire"
MAX_SIZE = 1200


def handler(incoming, context):
    s3 = boto3.client(
        's3',
        aws_access_key_id=os.environ.get('aws_access_key_id'),
        aws_secret_access_key=os.environ.get('aws_secret_access_key'),
        config=Config(signature_version='s3v4')
    )

    pusher_client = pusher.Pusher(
        app_id='488226',
        key=os.environ.get('pusher_key'),
        secret=os.environ.get('pusher_secret'),
        cluster='eu',
        ssl=True
    )

    event = incoming['body']
    if isinstance(incoming['body'], str):
        event = json.loads(incoming['body'])

    in_file = "inc." + event['ext']
    out_file = "out.jpeg"

    # Container reuse fix
    silent_remove(CAIRE)
    silent_remove(FAST_INC)
    silent_remove(out_file)
    silent_remove(in_file)
    #

    copyfile(str(os.curdir) + "/" + CAIRE, "/tmp/" + CAIRE)
    os.chmod("/tmp/" + CAIRE, 0o555)

    img_data = base64.b64decode(event['img'])
    img_file = open("/tmp/" + in_file, 'wb')
    img_file.write(img_data)
    img_file.close()

    if 'speedup' in event and event['speedup']:
        print("Speeding up")

        img = Image.open("/tmp/" + in_file)

        width, height = img.size
        if width > MAX_SIZE or height > MAX_SIZE:
            img.thumbnail((MAX_SIZE, MAX_SIZE))

        in_file = FAST_INC
        img.save("/tmp/" + in_file, format="PNG")

    params = ["/tmp/caire", "-in", "/tmp/" + in_file, "-out", "/tmp/" + out_file] + \
             event["params"] if type(event["params"]) is list else []

    img_id = str(uuid.uuid4())
    pusher_client.trigger(str(event["sessionId"]), 'img-in-progress', {'id': img_id})

    subprocess.Popen(params).wait()

    with open("/tmp/" + out_file, 'rb') as f:
        s3.upload_fileobj(f, 'budziol-imgs', img_id + ".jpeg", ExtraArgs={
            'ACL': 'public-read',
            'ContentType': 'image/jpeg'
        })

    url = "https://s3.eu-west-3.amazonaws.com/budziol-imgs/" + img_id + ".jpeg"
    print("Uploaded file. Url: " + url)
    pusher_client.trigger(str(event["sessionId"]), 'img-done', {'url': url, 'id': img_id})

    return {
        "statusCode": 200
    }


def silent_remove(name):
    try:
        os.remove("/tmp/" + name)
    except OSError:
        pass